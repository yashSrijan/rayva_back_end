//initialization file for the app
var path = require('path');
var express = require('express');
const nodemailer = require('nodemailer');
const bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var fs = require('fs');
var logger = require('morgan');
var xml2js = require('xml2js');
var template = require('./mailTemplate.js');
var parser = new xml2js.Parser({explicitArray : false, mergeAttrs : true });
var url = require('url') ;

//for sending sms using twilio
const authToken = '44172be8ea32a4926f77e1daca383398';
const accountSid = 'AC6d0fa06a1f6721fff327a60397cfddd1';
const client = require('twilio')(accountSid, authToken);

//create new express instance
var app = express();

app.set('port', 3002);
var port = app.get('port');
app.listen(port, function () {
     console.log(`App listening on port : ${port} !`);
});

var indexRouter = require('./routes/index');
app.use('/', indexRouter);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//use required middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false})   );
app.use( require('request-param')() );
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "content-type");
    res.header('Access-Control-Allow-Origin: *');
    res.header('Access-Control-Allow-Headers: X-Requested-With');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));




app.post('/api/form', (req,res) => {

    var hostname = req.headers.host; 
    hostname = 'http://' + hostname;
    console.log(hostname);
    console.log("--------------------------------------------------------------------------");

    const output = template(req.body, hostname);
    const receiver =    req.body['cEmail'] + ', '+ req.body['email'];

    let transporter = nodemailer.createTransport( {
        host: 'smtp.office365.com',
        port: 587, // secure SMTP
        secure: false,
        auth: {
            user: 'support@rayva.com',
            pass: 'RayvaTheater18#'
        },
        tls : {
            ciphers: 'SSLv3',
            rejectUnauthorized: false
        }, 
        requireTLS: true 
    })

    let mailOPtions = {
        from: '"Rayva" <support@rayva.com>',
        to: receiver,
        replyTo: 'support@rayva.com',
        subject: 'Rayva Theater Solution',
        html: output
    }

    transporter.sendMail(mailOPtions, (err, info) => {
        if(err) {
            console.log(err);
            res.send({ status:'Mail could not be sent !', 'errCause':err })
        }
        else {
            console.log("Message sent : " + info.response)
            res.send({ status:'Mail sent !' })
        }
    })
})

app.post('/api/sms', (req, res) => {
    //the following code is for twilio api
    client.messages.create({
        to : req.body['mobileNumber'],
        from : '+12018222955',
        body : 'Your Rayva Home Theater VR Link is : ' + req.body['phonevirtuallink']
    }).then( (message) => {
        console.log(message.sid) 
        res.send({ result:'success', 'msg':'Sent !' })
    })
    .catch( e => { 
        /*console.error('Got an error:', e.code, e.message);*/
        res.send({ result:'error', 'msg':'Not sent !' }) 
    });
})

//each file corresponding to a particular dealer should be placed in the dealerFiles folder in the root dir of project
app.get('/api/dealer', (req, res) => {
    let dealerId = req.param('dealerId');
    console.log("dealerId : ", dealerId);
    let flag = 0;
    fs.readFile('./dealerFiles/' + dealerId +'.xml', function(err, data) {
        if(err) {
            console.log("--------- error occured in finding the file ----------");
            res.status(200).send({dealer : null});
        } else {
            parser.parseString(data, function(err, obj) {
                if(err) {
                    res.status(404).send('Something went wrong !');
                }
                let dealer = obj;
                for (let key in dealer) {
                    if (dealer.hasOwnProperty(key) && key === dealerId) {
                       flag = 1;
                    }
                }
                if(flag === 1) {
                    res.status(200).send({dealer : dealer[dealerId]})
                } else {
                    res.status(200).send({dealer : null})
                }
            })
        }
    })
})

app.get('/api/password', (req, res) => {
    let provided_password = req.param('password');
    let provided_dealer = req.param('dealer');
    let flag = 0;

    fs.readFile('./dealerFiles/' + provided_dealer +'.xml', function(err, data) {
        if(err) {
            res.status(404).send('Something went wrong !');
        }
        parser.parseString(data, function(err, obj) {
            if(err) {
                res.status(404).send('Something went wrong !');
            }
            let jsonObj = JSON.parse(JSON.stringify(obj));
            if(jsonObj[provided_dealer]['password'] === provided_password) {
                flag = 1;
                // console.log(jsonObj[provided_dealer]);
            }
            if(flag === 1) {
                res.status(200).send({passwordStatus : true})
            } else {
                res.status(200).send({passwordStatus : false})
            }
        })
    })
})

app.get('/api/designthemes', (req, res) => {
    //a global design themes file is serving all the dealers
    //console.log("in designthemes api");
    fs.readFile('./dealerFiles/designthemes.xml', function(err, data) {
        if(err) {
            console.log("--------- error occured in finding the designthemes file ----------");
            res.status(200).send({designthemes : null});
        } else {
            parser.parseString(data, function(err, obj) {
                if(err) {
                    res.status(404).send('Something went wrong !');
                } else {
                    let designthemes = obj;
                    res.status(200).send({designthemes : designthemes});
                }
            })
        }
    })
})

module.exports = app;
