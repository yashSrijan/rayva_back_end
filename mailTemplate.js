const templateString = (body, hostname) => {
    let dealerlogo = body['firstname'].toLowerCase() === 'rayva' ? ' ' : `<tr ><td align="center" ><img src =  ${body['dealerlogo']} style = "margin : 10px auto; width : 60%;"  alt = "Dealer_Img" ></img></td></tr>`;
    return (
        `
        <!DOCTYPE html>
        <html>
            <head>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <style type="text/css">
            img{
                max-width:100%;
            }
            </style>
            </head>
            <body>
                <center>
                        <table style = "max-width: 680px;" cellspacing="0" cellpadding="0">
                            <tbody >
                                <!--///////////////// Rayva Image /////////////////-->
                                <tr >
                                    <td align="center" >
                                        <img src =  ${hostname + '/rayva_logo.png'} style = "margin : 10px auto; width : 80%;"  alt = "Rayva_Img" ></img>
                                    </td>
                                </tr>
                                <!--///////////////// Dealer Image /////////////////-->
                                ${dealerlogo}
                                <!--///////////////// Intro Text and HR /////////////////-->
                                <tr >
                                    <td align="center" >
                                        <div style = "max-width: 50%; font-size : 15px; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; color: rgb(44, 44, 44);">
                                            Dear ${body['cFname']}, Thank you for your interest in a Rayva Home Entertainment Solution
                                        </div>
                                        <hr style = "height : 3px; background: #554d4d; margin-bottom: 20px;"/>
                                    </td>
                                </tr>
                                <!--///////////////// Three Colums /////////////////-->
                                <tr >
                                    <td align="center">
                                        <table cellspacing="0" cellpadding="0" border="0" width = "100%" >
                                            <tbody>
                                                <colgroup>
                                                    <col style="width: 33%" />
                                                    <col style="width: 33%" />
                                                    <col style="width: 33%" />
                                                </colgroup>
                                                <tr>
                                                    <td valign = "top" align="center" style = "box-sizing: border-box; padding: 10px; text-align: center; background: #043e4e; border-right: 10px solid white">
                                                        
                                                            <div style = "color: yellow; font-size: 20px; ">1</div>
                                                            <div style = "color: yellow; font-size: 16px; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; padding-bottom:15px; transform: scale(1,.8); letter-spacing: 3px">
                                                                <span style="color: white;">ROOM SIZE</span><br/>${body['numofchairs']} chairs: ${body['currency']}${body['roomcost']}
                                                            </div>
                                                            <div style = "position:relative;">
                                                                <img width="150" src = ${hostname + '/roomsizes/' + body['roomsize'].replace(/ /g,'') + '.png'} alt="RoomSize"></img>
                                                                <!-- <img width="150" src = ${hostname + '/chairs/' + body['chairtype'] + '.png'} alt = "Chairtype"></img> -->
                                                            </div>
                                                        
                                                    </td>
                                                    <td valign = "top" align="center" style = "box-sizing: border-box; padding: 10px; text-align: center; background: #043e4e;">
                                                        
                                                            <div style = "color: yellow; font-size: 20px; ">2</div>
                                                            <div style = "color: yellow; font-size: 16px; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; padding-bottom:15px; transform: scale(1,.8); letter-spacing: 3px">
                                                                <span style="color: white;">AV SYSTEM</span><br/>${body['avsystem']}: ${body['currency']}${body['avsystemcost']}
                                                            </div>
                                                            <div style = "position:relative;">
                                                                <img src = ${hostname + '/avsystems/' + body['avsystem'] + '_.png'} alt="AVSystem"></img>
                                                            </div>
                                                        
                                                    </td>
                                                    <td valign = "top" align="center" style = "box-sizing: border-box; padding: 10px; text-align: center; background: #043e4e; border-left: 10px solid white">
                                                        
                                                            <div style = "color: yellow; font-size: 20px; ">3</div>
                                                            <div style = "color: yellow; font-size: 16px; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; padding-bottom:15px; transform: scale(1,.8); letter-spacing: 3px">
                                                                <span style="color: white;">DESIGN THEME &amp; ENGINEERING</span><br/>${body['designtheme']}: ${body['currency']}${body['designthemecost']}
                                                            </div>
                                                            <div style = "position:relative;">
                                                                <img src = ${hostname + '/designthemes/thumbnails/TN_' + body['designtheme'].replace(/ /g,'') + '.jpg'} alt="DesignTheme" width="180"></img>
                                                            </div>
                                                            
                                                    </td>
                                                <tr/>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!--///////////////// Price Text and HR /////////////////-->
                                <tr>
                                    <td align="center" >
                                        <div style = "padding-top: 10px; max-width: 50%; font-size : 18px; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; color: rgb(44, 44, 44);">
                                            YOUR TOTAL PRICE: ${body['currency']}${body['totalcost']}
                                        </div>
                                        <hr style = "height : 3px; background: #554d4d; margin-bottom: 20px;"/>
                                    </td>
                                </tr>
                                <!--///////////////// Customer and Dealer Info /////////////////-->
                                <tr>
                                    <td align="center">
                                        <table cellspacing="0" cellpadding="0" border="0" width = "100%">
                                            <tbody>
                                                <colgroup>
                                                    <col style="width: 50%" />
                                                    <col style="width: 50%" />
                                                </colgroup>
                                                <tr style = "font-size : 18px; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; color: rgb(44, 44, 44);">
                                                    <td align="left" style = "border-right : 20px solid white;">
                                                        <div style = "padding-bottom: 15px">
                                                            Customer Information:
                                                        </div>                                                
                                                    </td>
                                                    <td align="left"  style = "border-left : 20px solid white;">
                                                        <div style = "padding-bottom: 15px">
                                                            Proposal Created By:
                                                        </div>         
                                                    </td>
                                                <tr/>
                                                <tr style = "font-family : 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; color : black; background: rgb(175, 175, 175); ">
                                                    <td valign = "top" align = "left" style = "padding : 10px; border-right : 20px solid white;">
                                                        <ul style = "list-style-type : none; padding : 0; margin : 0; font-size : 15px; ">
                                                            <li style = "margin:0">Name : ${body['cFname']} ${body['cLname']}</li>
                                                            <li style = "margin:0">Address : ${body['cAddress1'] + (body['cAddress2'] ? ', ' + body['cAddress2'] : '')}, ${body['cCity']}, ${body['cState']}, ${body['cCountry']}</li>
                                                            <li style = "margin:0">Email : ${body['cEmail']}</li>
                                                            <li style = "margin:0">Phone : ${body['cPhone']}</li>
                                                            <li style = "margin:0">Room Size : ${body['roomsize']} (${body['numofchairs']} Seats)</li>
                                                            <li style = "margin:0">AV System : ${body['avsystem']} (${body['currency']}${body['avsystemcost']})</li>
                                                            <li style = "margin:0">Design Theme : ${body['designtheme']} (${body['currency']}${body['designthemecost']})</li>
                                                            <li style = "margin:0">Chair Type : ${body['chairtype']} - ${body['numofchairs']} Seats (${body['currency']}${body['roomcost']})</li>
                                                            <li style = "margin:0">Total Price : ${body['currency']}${body['totalcost']}</li>
                                                            <li style = "margin:0">URL for Virtual Tour : ${body['virtuallink']}</li>
                                                        <ul>
                                                    </td>
                                                    <td valign = "top" align = "left" style = "padding : 10px; border-left : 20px solid white;">
                                                        <ul style = "list-style-type : none; padding : 0; margin : 0; font-size : 15px; ">
                                                            <li style = "margin:0">Company Name : ${body['company']}</li>
                                                            <li style = "margin:0">Name : ${body['firstname']} ${body['lastname']}</li>
                                                            <li style = "margin:0">Address : ${body['address1'] + (body['address2'] ? ', ' + body['address2'] : '')}, ${body['city']}, ${body['state']}, ${body['country']}</li>
                                                            <li style = "margin:0">Email : ${body['email']}</li>
                                                            <li style = "margin:0">Phone : ${body['phone']}</li>
                                                        </ul>
                                                    </td>
                                                <tr/>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!--///////////////// HR /////////////////-->
                                <tr>
                                    <td align="center" >
                                        <hr style = "height : 3px; background: #554d4d; margin-bottom: 20px;"/>
                                    </td>
                                </tr>
                                <!--///////////////// Footer /////////////////-->
                                <tr style = "letter-spacing: 2px; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;">
                                    <td align="center">
                                        <table cellspacing="0" cellpadding="0" border="0" width = "97%">
                                            <tbody style = "background: #373537; border-radius: 1px">
                                            <!--///////////////// Greeting White Text /////////////////-->
                                            <tr>
                                                    <td align="center" >
                                                        <div style = "color:white; transform: scale(1,.8); font-size: 15px; margin: 10px 0">
                                                            TO FIND MORE ABOUT OUR THEATERS
                                                        </div>
                                                    </td>
                                            </tr>
                                            <!--///////////////// Contact Us /////////////////-->
                                            <tr>
                                                    <td align="center" >
                                                        <div style = "width:130px; border:1px solid yellow; padding:10px 16px; color: yellow; transform: scale(1,.9); display:inline-block; margin-bottom: 10px">
                                                            CONTACT US
                                                        </div>
                                                    </td>
                                            </tr>
                                            <!--///////////////// Social Icons /////////////////-->
                                            <tr>
                                                    <td align = "center">
                                                        <div style = "width:140px; display:inline-block;text-align:center">
                                                            <a  target = "_blank" href = "https://www.facebook.com/rayvahometheaters/"  style = "width:20%; padding-left:3px; margin-right:-4px; display:inline-block;"><img src =  ${hostname + '/emailTemplates/facebook.png'}  style="max-width:100%;"></img></a>
                                                            <a  target = "_blank" href = "https://twitter.com/rayvatheaters"   style = "width:20%; padding-left:3px; margin-right:-4px; display:inline-block;"><img src =  ${hostname + '/emailTemplates/twitter.png'}   style="max-width:100%;"></img></a>
                                                            <a  target = "_blank" href = "https://www.instagram.com/rayvahometheater/" style = "width:20%; padding-left:3px; margin-right:-4px; display:inline-block;"><img src =  ${hostname + '/emailTemplates/instagram.png'} style="max-width:100%;"></img></a>
                                                            <a  target = "_blank" href = "https://www.linkedin.com/company/rayva-international/"  style = "width:20%; padding-left:3px; margin-right:-4px; display:inline-block;"><img src =  ${hostname + '/emailTemplates/linkedin.png'}  style="max-width:100%;"></img></a>
                                                        </div>
                                                    </td>
                                            </tr>
                                            <!--///////////////// Rayva Phone Number /////////////////-->
                                            <tr>
                                                    <td align = "center">
                                                        <div style = "color:yellow; transform: scale(1,.8); font-size: 13px; margin: 18px 0">
                                                            RAYVA &copy; 2018 | INFO@RAYVA.COM | 844 467 2982
                                                        </div>
                                                    </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                </center>
            </body>
        </html>
        `
    );
};
module.exports = templateString;